# manifests

This repository contains kubernetes manifests for deploying our "application".

## Usage

> Make sure to use the correct context. e.g. `kubectl config use docker-desktop` when using K8s via Docker Desktop

### Deploy the "application"
```bash
kubectl apply -k ./application
```

To delete it again:
```bash
kubectl delete -k ./application
```
